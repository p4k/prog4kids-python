import turtle

t = turtle.Pen()

def dessine_polygone(nb_cote, long_cote):
    angle_externe = 360 / nb_cote
    for i in range(0, nb_cote):
        t.forward(long_cote)
        t.left(angle_externe)

def dessine_étoile(nb_cote, long_cote):
    angle_interne = ((nb_cote -2) * 180) / nb_cote
    for i in range(0, nb_cote):
        t.forward(long_cote)
        t.left(angle_interne)

long_cote = 40;
nb_cote = 3

print(t._position)

# dessine_étoile(6,150)


for x in range(0,15):
    dessine_polygone(nb_cote, long_cote)
    nb_cote = nb_cote +1




turtle.Screen().exitonclick()