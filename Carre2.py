import turtle

# On prépare la tortue
t = turtle.Pen()

# On définit la fonction qui dessine un carré
def dessine_un_carre(cote):
    for i in range(0,4):
        t.forward(cote)
        t.left(90)

# On définit une longueur de coté de base
cote = 10;

# On dessine 20 acrrés imbriqués de plus en plus grand
for x in range(0,20):
    dessine_un_carre(cote)
    cote = cote + 5

turtle.Screen().exitonclick()